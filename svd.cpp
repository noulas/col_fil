/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   svd.cpp
 * Author: noulas
 * 
 * Created on May 13, 2016, 1:15 PM
 */

#include "./svd.h"

// Constructors
svd::svd(gsl_spmatrix* model_data, size_t n_factors){
  data = model_data;
  user_factors = gsl_matrix_calloc(data->size1, n_factors);
  item_factors = gsl_matrix_calloc(n_factors, data->size2);
  user_adj = gsl_vector_calloc(data->size1);
  item_adj = gsl_vector_calloc(data->size2);
  // calculate MU immediately
  double sum = 0;
  for(size_t i = 0; i < data->nz; i++){
    sum += data->data[i];
  }
  mu = sum / data->nz;
  rmse_error = -1;
}

svd::svd(std::string file_path, size_t nrow, size_t ncol, size_t n_elements,
         size_t n_factors){
  data = gsl_spmatrix_alloc_nzmax(nrow, ncol, n_elements,
                                  GSL_SPMATRIX_TRIPLET);
  std::ifstream csv(file_path);
  size_t row;
  size_t col;
  double value;
  while(csv >> row >> col >> value){
    gsl_spmatrix_set(data, row, col, value);
  }
  user_factors = gsl_matrix_calloc(data->size1, n_factors);
  item_factors = gsl_matrix_calloc(n_factors, data->size2);
  user_adj = gsl_vector_calloc(data->size1);
  item_adj = gsl_vector_calloc(data->size2);
  // calculate MU immediately
  double sum = 0;
  for(size_t i = 0; i < data->nz; i++){
    sum += data->data[i];
  }
  mu = sum / data->nz;
  rmse_error = -1;
}

// Prediction methods
double svd::predict(const size_t &user, const size_t &item){
  double user_item_score;
  gsl_vector_view user_factor = gsl_matrix_row(user_factors, user);
  gsl_vector_view item_factor = gsl_matrix_column(item_factors, item);
  double user_baseline = gsl_vector_get(user_adj, user);
  double item_baseline = gsl_vector_get(item_adj, item);
  gsl_blas_ddot(&user_factor.vector, &item_factor.vector, &user_item_score);
  double pred = mu + user_baseline + item_baseline + user_item_score;
  return pred;
}

gsl_spmatrix* svd::predict(){
  // loop through all existing ratings
  // model.data->i[k] denotes user `u`
  // model.data->p[k] denotes item `i`
  // model.data->data[k] denotes rating for user `u` of item `i`
  size_t user;
  size_t item;
  gsl_spmatrix* preds = gsl_spmatrix_alloc_nzmax(data->size1,
                                                 data->size2,
                                                 data->nz,
                                                 GSL_SPMATRIX_TRIPLET);
  for(size_t k = 0; k < data->nz; k++){
    user = data->i[k];
    item = data->p[k];
    gsl_spmatrix_set(preds, user, item, predict(user, item));
  }
  return preds;
}

gsl_spmatrix* svd::predict(gsl_spmatrix* new_data){
  // loop through all existing ratings
  // model.data->i[k] denotes user `u`
  // model.data->p[k] denotes item `i`
  // model.data->data[k] denotes rating for user `u` of item `i`
  size_t user;
  size_t item;
  gsl_spmatrix* preds = gsl_spmatrix_alloc_nzmax(new_data->size1,
                                                 new_data->size2,
                                                 new_data->nz,
                                                 GSL_SPMATRIX_TRIPLET);
  for(size_t k = 0; k < new_data->nz; k++){
    user = new_data->i[k];
    item = new_data->p[k];
    gsl_spmatrix_set(preds, user, item, predict(user, item));
  }
  return preds;
}

// Error methods
gsl_spmatrix* svd::getErrors(){
  // Predict all ratings and then we can just subtract from the actuals
  size_t user;
  size_t item;
  double actual;
  double pred;
  gsl_spmatrix* preds = predict();
  gsl_spmatrix* errors = gsl_spmatrix_alloc_nzmax(data->size1,
                                                  data->size2,
                                                  data->nz,
                                                  GSL_SPMATRIX_TRIPLET);
  for(size_t k = 0; k < data->nz; k++){
    user = data->i[k];
    item = data->p[k];
    actual = gsl_spmatrix_get(data, user, item);
    pred = gsl_spmatrix_get(preds, user, item);
    gsl_spmatrix_set(errors, user, item, actual - pred);
  }
  gsl_spmatrix_free(preds);
  return errors;
}

double svd::rmse(gsl_spmatrix* errors){
  double sum = 0;
  for(size_t k = 0; k < errors->nz; k++){
    sum += errors->data[k] * errors->data[k];
  }
  return sqrt(sum / errors->nz);
}

double svd::rmse(){
  gsl_spmatrix* errors = getErrors();
  double out = rmse(errors);
  gsl_spmatrix_free(errors);
  return out;
}

// Model building functions
void svd::build(size_t n_iter, double lr_1, double lr_2,
                double l_1, double l_2){

  for(size_t i = 0; i < n_iter; i++){
    if(old_error != -1 && old_error < rmse_error)
    {
        lr_1*=0.8;
        lr_2*=0.8;
        l_1*=0.8;
        l_2*=0.8;
        std::cout << "Lowered learning rates " << rmse_error << " from " << old_error << std::endl;
    }
    old_error = rmse_error;
    update(lr_1, lr_2, l_1, l_2);
    
  }
}

void svd::update(const double &lr_1, const double &lr_2,
                 const double &l_1, const double &l_2){
  gsl_spmatrix* errors = getErrors();
  size_t user;
  size_t item;
  double error;
  gsl_vector_view user_factor;
  gsl_vector_view item_factor;
  gsl_vector* lr_1_user_v = gsl_vector_alloc(data->size1);
  gsl_vector* lr_2_user_v = gsl_vector_alloc(data->size1);
  gsl_vector* lr_1_item_v = gsl_vector_alloc(data->size2);
  gsl_vector* lr_2_item_v = gsl_vector_alloc(data->size2);
  gsl_vector_set_all(lr_1_user_v, lr_1);
  gsl_vector_set_all(lr_1_item_v, lr_1);
  gsl_vector_set_all(lr_2_user_v, lr_2);
  gsl_vector_set_all(lr_2_item_v, lr_2);
  rmse_error = rmse(errors);
  std::cout << "Current RMSE: " << rmse_error << "\n";
  // Experimenting with random ordering of elements
  std::vector<size_t> elements(errors->nz);
  for(size_t i = 0; i < elements.size(); i++) elements[i] = i;
  std::random_shuffle(elements.begin(), elements.end());
  size_t k;
  for(size_t l = 0; l < elements.size(); l++){
    k = elements[l];
    user = errors->i[k];
    item = errors->p[k];
    error = errors->data[k];
    user_factor = gsl_matrix_row(user_factors, user);
    item_factor = gsl_matrix_column(item_factors, item);
    updateAdjustment(user_adj, lr_1_user_v, l_1, error, user);
    updateAdjustment(item_adj, lr_1_item_v, l_1, error, item);
    updateLatentFactor(user_factor, lr_2_user_v, l_2, error, user);
    updateLatentFactor(item_factor, lr_2_item_v, l_2, error, item);
  }
  gsl_vector_free(lr_1_user_v);
  gsl_vector_free(lr_2_user_v);
  gsl_vector_free(lr_1_item_v);
  gsl_vector_free(lr_2_item_v);
  gsl_spmatrix_free(errors);
}

void svd::updateLatentFactor(gsl_vector_view &latent_factor, gsl_vector* lr_2_v,
                             const double &l_2, const double &error,
                             const size_t &element){
  //const double STEP_SIZE_UPDATE = 0.9;
  const double lr_2 = gsl_vector_get(lr_2_v, element);
  gsl_vector* latent_factor_d = &latent_factor.vector;
  gsl_vector* latent_factor_copy = gsl_vector_alloc(latent_factor_d->size);
  gsl_vector_memcpy(latent_factor_copy, latent_factor_d);
  gsl_blas_dscal(-l_2, latent_factor_copy);
  gsl_blas_daxpy(error, latent_factor_d, latent_factor_copy);
  gsl_blas_daxpy(lr_2, latent_factor_copy, latent_factor_d);
  gsl_vector_free(latent_factor_copy);
  gsl_vector_set(lr_2_v, element, lr_2 * 1);
}

void svd::updateAdjustment(gsl_vector* adj, gsl_vector* lr_1_v,
                           const double &l_1, const double &error,
                           const size_t &element){
  //const double STEP_SIZE_UPDATE = 0.9;
  const double current = gsl_vector_get(adj, element);
  const double lr_1 = gsl_vector_get(lr_1_v, element);
  gsl_vector_set(adj, element, current + lr_1 * (error - l_1 * current));
  gsl_vector_set(lr_1_v, element, lr_1 * 1);
}

// Getters
gsl_spmatrix* svd::getData(){
  return data;
}

gsl_matrix* svd::getUserFactors(){
  return user_factors;
}

gsl_matrix* svd::getItemFactors(){
  return item_factors;
}

gsl_vector* svd::getUserAdjustments(){
  return user_adj;
}

gsl_vector* svd::getItemAdjustments(){
  return item_adj;
}

double svd::getMu(){
  return mu;
}