/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: noulas
 *
 * Created on May 13, 2016, 1:13 PM
 */
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <iostream>
#include <gsl/gsl_rng.h>


#include "./svd.h"

int main(){
//  svd model = svd("./data/training_data_post_processing.txt",
//                  28978, 1821, 3255352, 100);
//  model.build(30, 0.01, 0.01, 0.005, 0.015);
//  std::cout << "User Adjustment Ex. "
//            << gsl_vector_get(model.getUserAdjustments(), 10) << "\n";
//  std::cout << "Item Adjustment Ex. "
//            << gsl_vector_get(model.getItemAdjustments(), 10) << "\n";
//  
//unit test
  size_t n_users = 1000;
  size_t n_items = 1000;
  size_t n_rank = 5;
  gsl_matrix* user_data = gsl_matrix_alloc(n_users, n_rank);
  gsl_matrix* item_data = gsl_matrix_alloc(n_items, n_rank);
  
  
  const gsl_rng_type * T;
  gsl_rng * r;

  int i, n = 10;

  gsl_rng_env_setup();

  T = gsl_rng_default;
  r = gsl_rng_alloc (T);
  for(size_t d = 0; d < n_rank; ++d)
  {
      for(size_t i = 0; i < n_users; ++i)
      {
          gsl_matrix_set(user_data, i, d, gsl_rng_uniform (r));
      }
      for(size_t i = 0; i < n_items; ++i)
      {
          gsl_matrix_set(item_data, i, d, gsl_rng_uniform (r));
      }
  } 
  gsl_matrix* design_matrix = gsl_matrix_alloc(n_users, n_items);
  
  std::cout << "Multiplying " << std::endl;  
  gsl_blas_dgemm(CblasNoTrans,CblasTrans,1, user_data, item_data, 0.0, design_matrix);
  std::cout << "Done " << std::endl;
  
  gsl_spmatrix* S = gsl_spmatrix_alloc(n_users,n_items);
  
  gsl_spmatrix_d2sp (S, design_matrix);
  svd my_svd(S, n_rank);
  my_svd.build(3000, 0.001, 0.001, 0.001, 0.001);

  
  return 0;
}