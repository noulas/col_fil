/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   svd.h
 * Author: noulas
 *
 * Created on May 13, 2016, 1:15 PM
 */

#ifndef SVD_H
#define SVD_H


#include <gsl/gsl_spmatrix.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <fstream>
#include <iostream>
#include <algorithm> // std::random_shuffle
#include <vector> // std::vector
#include <math.h> // sqrt

class svd{
  // Model params
  gsl_spmatrix* data;
  gsl_matrix* user_factors;
  gsl_matrix* item_factors;
  gsl_vector* user_adj;
  gsl_vector* item_adj;
  double mu;
  double step_adj;
  double rmse_error;
  double old_error;
  // Building methods
  void update(const double &lr_1, const double &lr_2,
              const double &l_1, const double &l_2);
  void updateLatentFactor(gsl_vector_view &latent_factor, gsl_vector* lr_2_v,
                          const double &l_2, const double &error,
                          const size_t &element);
  void updateAdjustment(gsl_vector* adj, gsl_vector* lr_1_v,
                        const double &l_1, const double &error,
                        const size_t &element);
  // Evaluation
  double rmse(gsl_spmatrix* errors);
 public:
  // Constructors
  svd(gsl_spmatrix* data, size_t n_factors);
  svd(std::string file_path, size_t nrow, size_t ncol, size_t n_elements,
      size_t n_factors);
  // Build Method
  void build(size_t n_iter, double lr_1, double lr_2,
             double l_1, double l_2);
  // Analyze errors
  gsl_spmatrix* getErrors();
  double rmse();
  // Prediction methods
  double predict(const size_t &user, const size_t &item);
  gsl_spmatrix* predict();
  gsl_spmatrix* predict(gsl_spmatrix* new_data);
  // Getters
  gsl_spmatrix* getData();
  gsl_matrix* getUserFactors();
  gsl_matrix* getItemFactors();
  gsl_vector* getUserAdjustments();
  gsl_vector* getItemAdjustments();
  double getMu();
};

#endif /* SVD_H */

